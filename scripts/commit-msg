#!/bin/bash
#
# A hook script to check the commit log messages. Called by "git push".
# The hook should exit with non-zero status after issuing an
# appropriate message if it wants to stop the push.

# This script validate the commit messages convention.

readonly HEADER_PATTERN="^(.+): (.+)$"
readonly TYPE_PATTERN="^(feat|fix|style|refactor|perf|test|docs|chore|version|ops|defaults)$"
readonly SUBJECT_PATTERN_CAPITALIZE="^[A-Z]"
readonly SUBJECT_PATTERN_PERIOD="[^ ^\.]$"
readonly TRAILING_SPACE_PATTERN=" +$"

ERROR_BLANK_LINE=false
ERROR_HEADER_LENGTH=false
ERROR_SUBJECT_CAPITALIZE=false
ERROR_SUBJECT_PERIOD=false
ERROR_BODY_LENGTH=false
ERROR_STRUCTURE=false

GLOBAL_HEADER=""
GLOBAL_TYPE=""
GLOBAL_SUBJECT=""
GLOBAL_BODY=""
GLOBAL_FOOTER=""

GLOBAL_HEADER_MAX_LENGTH=50
GLOBAL_BODY_MAX_LENGTH=72

validate_overall_structure() {
  local MESSAGE="$1"

  local WAITING_HEADER=0
  local WAITING_EMPTY=1
  local START_TEXT=2
  local READING_BODY=3
  local READING_FOOTER=4

  local STATE="$WAITING_HEADER"

  while IFS= read -r LINE; do

    if [[ $STATE -eq $WAITING_HEADER ]]; then
      GLOBAL_HEADER="$LINE"
      STATE="$WAITING_EMPTY"

    elif [[ $STATE -eq $WAITING_EMPTY ]]; then
      if [[ $LINE != "" ]]; then
        echo >&2 "ERROR: missing empty line in commit message between header and body or body and footer"
        ERROR_BLANK_LINE=true
      fi
      STATE="$START_TEXT"

    elif [[ $STATE -eq $START_TEXT ]]; then
      if [[ $LINE = "" ]]; then
        echo >&2 "ERROR: double empty line is not allowed"
        ERROR_STRUCTURE=true
      fi

      STATE="$READING_BODY"
      GLOBAL_BODY=$GLOBAL_BODY$LINE$'\n'

    elif [[ $STATE -eq $READING_BODY ]]; then
      if [[ $LINE = "" ]]; then
        STATE=$START_TEXT
      else
        GLOBAL_BODY=$GLOBAL_BODY$LINE$'\n'
      fi

    elif [[ $STATE -eq $READING_FOOTER ]]; then
      if [[ $LINE = "" ]]; then
        echo >&2 "ERROR: no empty line allowed"
        ERROR_STRUCTURE=true
      fi

      GLOBAL_FOOTER=$GLOBAL_FOOTER$LINE$'\n'

    else
      echo >&2 "ERROR: unknown state in parsing machine"
      ERROR_STRUCTURE=true
    fi

  done <<<"$MESSAGE"

  if [[ $STATE -eq $START_TEXT ]]; then
    echo >&2 "ERROR: new line at the end of the commit is not allowed"
    ERROR_STRUCTURE=true
  fi
}

validate_header() {
  local HEADER="$1"

  if [[ $HEADER =~ $HEADER_PATTERN ]]; then
    GLOBAL_TYPE=${BASH_REMATCH[1]}
    GLOBAL_SUBJECT=${BASH_REMATCH[2]}
  else
    echo >&2 "ERROR: commit header doesn't match overall header pattern: '<type>: <subject>'"
    ERROR_STRUCTURE=true
  fi
}

validate_header_length() {
  local HEADER="$1"
  if [[ ${#HEADER} -gt ${GLOBAL_HEADER_MAX_LENGTH} ]]; then
    echo >&2 "ERROR: commit header length is more than ${GLOBAL_HEADER_MAX_LENGTH} characters"
    ERROR_HEADER_LENGTH=true
  fi
}

validate_type() {
  local TYPE=$1

  if [[ $TYPE && ! $TYPE =~ $TYPE_PATTERN ]]; then
    echo >&2 "ERROR: commit type '$TYPE' is unknown"
    ERROR_STRUCTURE=true
  fi
}

validate_subject() {
  local SUBJECT=$1

  if [[ $SUBJECT && ! $SUBJECT =~ $SUBJECT_PATTERN_CAPITALIZE ]]; then
    echo >&2 "ERROR: commit subject '$SUBJECT' should start with a upper case"
    ERROR_SUBJECT_CAPITALIZE=true
  elif [[ $SUBJECT && ! $SUBJECT =~ $SUBJECT_PATTERN_PERIOD ]]; then
    echo >&2 "ERROR: commit subject '$SUBJECT' should not end with a '.'"
    ERROR_SUBJECT_PERIOD=true
  fi
}

validate_body_length() {
  local BODY=$1
  local LINE=""

  while IFS= read -r LINE; do
    if [[ ${#LINE} -gt $GLOBAL_BODY_MAX_LENGTH ]]; then
      echo >&2 "ERROR: body message line length is more than $GLOBAL_BODY_MAX_LENGTH charaters"
      ERROR_BODY_LENGTH=true
    fi
  done <<<"$BODY"
}

validate_trailing_space() {
  local BODY=$1
  local LINE=""

  while IFS= read -r LINE; do
    if [[ $LINE =~ $TRAILING_SPACE_PATTERN ]]; then
      echo >&2 "ERROR: body message must not have trailing spaces"
      ERROR_STRUCTURE=true
    fi
  done <<<"$BODY"
}

handle_error() {
  red='\033[0;31m'
  green='\033[0;32m'
  yellow='\033[0;33m'
  nc='\033[0m'
  failed="FAILED"
  passed="PASSED"
  status=""

  if $ERROR_STRUCTURE || $ERROR_BLANK_LINE || $ERROR_HEADER_LENGTH || $ERROR_SUBJECT_CAPITALIZE ||
    $ERROR_SUBJECT_PERIOD || $ERROR_BODY_LENGTH; then

    echo >&2 -e "\nConformance to the 7 rules of a great Git commit message:"

    if $ERROR_BLANK_LINE; then
      status="${red}${failed}${nc}"
    else
      status="${green}${passed}${nc}"
    fi
    echo >&2 -e "[$status] Separate subject from body with a blank line"

    if $ERROR_HEADER_LENGTH; then
      status="${red}${failed}${nc}"
    else
      status="${green}${passed}${nc}"
    fi
    echo >&2 -e "[$status] Limit the subject line to 50 characters"

    if $ERROR_SUBJECT_CAPITALIZE; then
      status="${red}${failed}${nc}"
    else
      status="${green}${passed}${nc}"
    fi
    echo >&2 -e "[$status] Capitalize the subject line"

    if $ERROR_SUBJECT_PERIOD; then
      status="${red}${failed}${nc}"
    else
      status="${green}${passed}${nc}"
    fi
    echo >&2 -e "[$status] Do not end the subject line with a period"

    echo >&2 -e "[$yellow  NA  $nc] Use the imperative mood in the subject line"

    if $ERROR_BODY_LENGTH; then
      status="${red}${failed}${nc}"
    else
      status="${green}${passed}${nc}"
    fi
    echo >&2 -e "[$status] Wrap the body at 72 characters"

    echo >&2 -e "[$yellow  NA  $nc] Use the body to explain what and why vs. how"

    if $ERROR_STRUCTURE; then
      status="${red}${failed}${nc}"
      echo >&2 -e "[$status] Structure"
    fi

    exit 1
  fi
}

validate() {
  local COMMIT_MSG="$1"

  if [[ $COMMIT_MSG == 'Merge'* ]] || [[ $COMMIT_MSG == 'Revert'* ]]; then
    return
  fi

  validate_overall_structure "$COMMIT_MSG"

  local HEADER="$GLOBAL_HEADER"
  local BODY="$GLOBAL_BODY"
  local FOOTER="$GLOBAL_FOOTER"

  validate_header "$HEADER"
  validate_header_length "$HEADER"

  local TYPE="$GLOBAL_TYPE"
  local SUBJECT="$GLOBAL_SUBJECT"

  validate_type "$TYPE"
  validate_subject "$SUBJECT"

  validate_body_length "$BODY"
  validate_body_length "$FOOTER"

  validate_trailing_space "$BODY"
  validate_trailing_space "$FOOTER"

  handle_error
}

COMMIT_MSG_FILE=$1
MESSAGE=`grep -v '^#' $COMMIT_MSG_FILE`

validate "$MESSAGE"
